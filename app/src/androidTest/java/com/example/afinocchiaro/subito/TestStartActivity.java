package com.example.afinocchiaro.subito;

import android.os.SystemClock;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.example.afinocchiaro.subito.scene.ui.start.StartActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.is;

/**
 * Created by afinocchiaro on 10/02/2018.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class TestStartActivity {

    String owner;
    String repo;

    @Rule
    public ActivityTestRule<StartActivity> mActivityRule = new ActivityTestRule<>(
            StartActivity.class);

    @Before
    public void setTextOnSearchViewOwner() {
        // Specify a valid string.
        owner = "airbnb";
        repo = "lottie-android";
        onView(withId(R.id.search_owner)).perform(typeSearchViewText(owner));
    }


    @Test
    public void setTextOnSearchViewRepo() {
        onView(withId(R.id.search_repo)).perform(setTextViewVisibitity(true));
        onView(withId(R.id.search_repo)).perform(typeSearchViewText(repo));
        SystemClock.sleep(5000);
        testClickItem();
    }




    public void testClickItem(){
        ViewInteraction recyclerView = onView(
                Matchers.allOf(withId(R.id.rv_stargazers),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                3)));
        recyclerView.perform(actionOnItemAtPosition(1, click()));
    }

    public static ViewAction typeSearchViewText(final String text) {

        return new ViewAction() {

            @Override

            public Matcher<View> getConstraints() {

                //Ensure that only apply if it is a SearchView and if it is visible.

                return allOf(isDisplayed(), isAssignableFrom(SearchView.class));

            }

            @Override

            public String getDescription() {
                return "Change view text";
            }

            @Override

            public void perform(UiController uiController, View view) {
                ((SearchView) view).setQuery(text, true);

            }

        };
    }


    private static ViewAction setTextViewVisibitity(final boolean value) {
        return new ViewAction() {

            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(SearchView.class);
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.setVisibility(value ? View.VISIBLE : View.GONE);
            }

            @Override
            public String getDescription() {
                return "Show / Hide View";
            }
        };
    }


        public static ViewAction clickChildViewWithId(final int id) {
            return new ViewAction() {
                @Override
                public Matcher<View> getConstraints() {
                    return null;
                }

                @Override
                public String getDescription() {
                    return "Click on a child view with specified id.";
                }

                @Override
                public void perform(UiController uiController, View view) {
                    View v = view.findViewById(id);
                    v.performClick();
                }
            };
        }


    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

}