package com.example.afinocchiaro.subito

import android.util.Log
import com.example.afinocchiaro.subito.network.StargazersNetworkLayer
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

/**
 * Created by afinocchiaro on 06/02/2018.
 */

class TestNetwork {

    lateinit var owner: String
    lateinit var repo: String

    @Before
    fun initialize() {

        owner = "airbnb"
        repo = "lottie-android"
    }


    /**
     * It is an integration test for check the API
     */
    @Test
    fun test() {

        val signal = CountDownLatch(1)
        StargazersNetworkLayer.getAllStargazers(owner, repo, onSuccess = {
            Assert.assertTrue(it.get(0).avatarUrl != null)
            Log.i("CIAO", "OK")
            signal.countDown()
        }, onFailure = {
            Log.i("CIAO", "FAIL")
            Assert.fail()
            signal.countDown()
        })

        try {
            signal.await()
        } catch (i: InterruptedException) {
            i.printStackTrace()
        }


    }
}
