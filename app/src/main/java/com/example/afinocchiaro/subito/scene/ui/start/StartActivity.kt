package com.example.afinocchiaro.subito.scene.ui.start

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.view.View
import android.view.View.*
import android.widget.ImageView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.afinocchiaro.axaprototype.BaseActivity
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.di.Injection
import com.example.afinocchiaro.subito.models.Stargazers
import com.example.afinocchiaro.subito.scene.ui.detail.DetailActivity
import com.example.afinocchiaro.subito.scene.ui.start.adapter.GridViewAdapter
import com.example.afinocchiaro.subito.scene.util.GraphicsUtils
import kotlinx.android.synthetic.main.activity_start.*
import kotlinx.android.synthetic.main.content_start.*

class StartActivity : BaseActivity(), StartContract.StartView {

    lateinit var presenter: StartPresenter
    lateinit var adapter: GridViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        setSupportActionBar(toolbar)
        presenter = StartPresenter(this, Injection.provideRepository())
        rv_stargazers.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 2)
        rv_stargazers.layoutManager = layoutManager
        adapter = GridViewAdapter(this)
        rv_stargazers.setItemViewCacheSize(20)
        rv_stargazers.isDrawingCacheEnabled = true
        rv_stargazers.drawingCacheQuality = DRAWING_CACHE_QUALITY_AUTO
        initialize()
    }


    override fun setPresenter(presenter: StartContract.StartPresenter) {
        this.presenter = presenter as StartPresenter
    }

    override fun updateView(stargazers: List<Stargazers>) {
        progress.visibility = GONE
        adapter.stargazersList = stargazers
        rv_stargazers.adapter = this.adapter
    }

    override fun showError(error: String) {
        progress.visibility = GONE
        GraphicsUtils.showGenericPopUp(this, error)
    }


    /**
     * Listener for search view
     */
    fun initialize() {
        search_owner.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search_repo.visibility = VISIBLE
                YoYo.with(Techniques.BounceInDown)
                        .duration(700)
                        .playOn(search_repo)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

        var closeButton: ImageView = search_repo.findViewById(R.id.search_close_btn)
        closeButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                YoYo.with(Techniques.FadeOut)
                        .duration(700)
                        .playOn(search_repo)
                search_repo.setQuery("", false)
            }
        })

        search_repo.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                progress.visibility = VISIBLE
                presenter.getAllStargazers(search_owner.query.toString(), search_repo.query.toString())
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return false
            }
        })
    }


    /**
     * It is a lambda function for change Activity
     */
    val gotoDetailActivity = { stargazers: Stargazers, imageView: ImageView ->
        var intent = Intent(baseContext, DetailActivity::class.java)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                imageView,
                ViewCompat.getTransitionName(imageView))
        intent.putExtra(getString(R.string.detail), stargazers)
        startActivity(intent, options.toBundle())
    }

}

