package com.example.afinocchiaro.axaprototype

/**
 * Created by afinocchiaro on 10/01/2018.
 */

interface BaseView<T> {
    fun setPresenter(presenter: T)
}
