package com.example.afinocchiaro.subito.network.inteface

import com.example.afinocchiaro.subito.models.Stargazers
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by afinocchiaro on 06/02/2018.
 */

 interface NetworkInterface {

    @GET("/repos/{owner}/{repo}/stargazers")
    open fun getallStargazers( @Path(value = "owner") owner: String,@Path(value = "repo") repo: String): Call<List<Stargazers>>

}
