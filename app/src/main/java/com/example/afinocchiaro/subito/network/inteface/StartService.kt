package com.example.afinocchiaro.subito.network.inteface

import com.example.afinocchiaro.subito.models.Stargazers

/**
 * Created by afinocchiaro on 11/01/2018.
 */

interface StartService {
    fun getAllStargazers(owner: String, repo: String, onSuccess: (List<Stargazers>) -> Unit, onFailure: (String) -> Unit)
}
