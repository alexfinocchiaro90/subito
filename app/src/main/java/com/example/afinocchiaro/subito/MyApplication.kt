package com.example.afinocchiaro.subito

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by afinocchiaro on 06/02/2018.
 */

open class MyApplication : Application() {

    companion object {
        var INSTANCE: MyApplication? = null
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        Realm.init(applicationContext)
        val config = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(config)
    }
}
