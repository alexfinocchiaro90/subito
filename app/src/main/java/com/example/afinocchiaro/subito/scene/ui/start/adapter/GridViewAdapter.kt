package com.example.afinocchiaro.subito.scene.ui.start.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.afinocchiaro.subito.MyApplication
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.models.Stargazers
import com.example.afinocchiaro.subito.scene.ui.start.StartActivity
import com.example.afinocchiaro.subito.scene.util.GraphicsUtils
import kotlinx.android.synthetic.main.star_grid_item.view.*

/**
 * Created by afinocchiaro on 07/02/2018.
 */
class GridViewAdapter(val activity: StartActivity) : RecyclerView.Adapter<GridViewAdapter.GridViewHolder>() {

    var stargazersList: List<Stargazers>? = null

    override fun getItemCount(): Int {
        return stargazersList?.size ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GridViewHolder {
        val context = parent?.getContext()
        val layoutIdForListItem = R.layout.star_grid_item
        val inflater = LayoutInflater.from(context)
        val shouldAttachToParentImmediately = false
        val view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately)
        return GridViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridViewHolder?, position: Int) {
        holder?.bind(activity.gotoDetailActivity, holder.itemView, stargazersList!!.get(position))
    }


    class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(click: (Stargazers, ImageView)   -> Unit, itemView: View, stargazers: Stargazers) {
            if (itemView != null && stargazers.avatarUrl != null) {
                GraphicsUtils.loadImage(MyApplication.INSTANCE!!.applicationContext,
                        stargazers.avatarUrl.toString(),
                        itemView.iv_avatar)
            }
            itemView.setOnClickListener {
                click(stargazers,itemView.iv_avatar)
            }
        }
    }


}