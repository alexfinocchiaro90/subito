package com.example.afinocchiaro.subito.network

import com.example.afinocchiaro.subito.MyApplication
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.network.inteface.NetworkInterface
import com.google.gson.Gson
import com.google.gson.GsonBuilder

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by afinocchiaro on 06/02/2018.
 */


class NetworkModule {

    companion object {

        private var retrofit: Retrofit? = null
        private var gson: Gson? = null
        private var networkInterface: NetworkInterface? = null

    }


    fun getClient(): Retrofit {

        if (retrofit == null) {

            retrofit = Retrofit.Builder()
                    .baseUrl(MyApplication.INSTANCE?.getString(R.string.baseurl))
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build()
        }

        return retrofit!!
    }


    fun getNetworkClient(): NetworkInterface {

        if (networkInterface == null) {
            networkInterface = getClient().create(NetworkInterface::class.java)
        }
        return networkInterface!!
    }


    fun getGson(): Gson {
        if (gson == null) {
            gson = GsonBuilder().create()
        }
        return gson!!
    }


}
