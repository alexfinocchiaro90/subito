package com.example.afinocchiaro.subito.network

import com.example.afinocchiaro.subito.MyApplication
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.models.Stargazers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 * Created by afinocchiaro on 06/02/2018.
 */

object StargazersNetworkLayer {

    fun getAllStargazers(owner: String, repo: String, onSuccess: (List<Stargazers>) -> Unit, onFailure: (String) -> Unit) {

        val call = NetworkModule().getNetworkClient().getallStargazers(owner, repo)
        call.enqueue(object : Callback<List<Stargazers>> {
            override fun onResponse(call: Call<List<Stargazers>>, response: Response<List<Stargazers>>) {
                if (response.isSuccessful) {
                    when (response.code()) {
                        200 ->
                            onSuccess(response.body()!!)
                        else ->
                            if (response.errorBody() != null) {
                                try {
                                    val jsonString = response.errorBody().toString()
                                    onFailure(jsonString)
                                } catch (e: IOException) {
                                    e.printStackTrace()
                                }
                            }
                    }
                } else {
                    try {
                        if(response.code() == 404)
                        onFailure(MyApplication.Companion.INSTANCE!!.getString(R.string.error_serch))
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<List<Stargazers>>, t: Throwable) {
                onFailure(MyApplication.INSTANCE!!.getString(R.string.error_msg))
            }
        })


    }


}
