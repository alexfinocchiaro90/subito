package com.example.afinocchiaro.subito.di

import com.example.afinocchiaro.subito.network.StartRepository
import com.example.afinocchiaro.subito.network.StartServiceImpl

/**
 * Created by afinocchiaro on 11/01/2018.
 */

object Injection {

    fun provideRepository(): StartRepository {
        return StartRepository.getInstance(StartServiceImpl())
    }

}
