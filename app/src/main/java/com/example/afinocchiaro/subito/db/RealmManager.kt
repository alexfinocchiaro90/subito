package com.example.afinocchiaro.subito.db

import com.example.afinocchiaro.subito.models.Stargazers
import io.realm.Realm

/**
 * Created by afinocchiaro on 12/01/2018.
 */

class RealmManager : RealmManagerInteface {


    override fun saveallStargazers(stargazersList: List<Stargazers>) {

        var realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(stargazersList)
        realm.commitTransaction()
    }

    override fun retriveallStargazers(): List<Stargazers> {
        var realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val query = realm.where(Stargazers::class.java!!)
        val list = query.findAll()
        realm.commitTransaction()
        return list
    }

    override fun flushRealm() {
        var realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        Realm.getDefaultInstance().deleteAll()
        realm.commitTransaction()
    }

    companion object {

        private var myInstance: RealmManager? = null

        val instance: RealmManager
            @Synchronized get() {
                if (myInstance == null) {
                    myInstance = RealmManager()
                }
                return myInstance!!
            }
    }
}
