package com.example.afinocchiaro.subito.network

import com.example.afinocchiaro.subito.models.Stargazers
import com.example.afinocchiaro.subito.network.inteface.StartService

/**
 * Created by afinocchiaro on 10/01/2018.
 *
 *
 * Repository
 */

class StartRepository private constructor(private val service: StartService) {
    companion object {
        private var instance: StartRepository? = null

        @Synchronized
        fun getInstance(service: StartService): StartRepository {
            if (instance == null) {
                instance = StartRepository(service)
            }
            return instance as StartRepository
        }
    }

    fun retriveallStargazers(owner: String, repo: String, onSuccess: (List<Stargazers>) -> Unit, onFailure: (String) -> Unit) {

        instance!!.service.getAllStargazers(owner, repo, onSuccess = {
            onSuccess(it)
        }, onFailure = {
            onFailure(it)
        })
    }


}
