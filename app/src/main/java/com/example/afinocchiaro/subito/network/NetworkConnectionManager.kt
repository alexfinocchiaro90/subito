package com.example.afinocchiaro.subito.network

import android.content.Context
import android.net.ConnectivityManager
import com.example.afinocchiaro.subito.MyApplication

/**
 * Created by afinocchiaro on 11/01/2018.
 */

object NetworkConnectionManager {

    val isOnline: Boolean
        get() {
            val cm = MyApplication.INSTANCE?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return if (netInfo != null && netInfo.isConnectedOrConnecting) {
                true
            } else {
               return false
            }
        }

}
