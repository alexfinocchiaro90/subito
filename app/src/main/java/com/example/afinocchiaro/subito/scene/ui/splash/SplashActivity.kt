package com.example.afinocchiaro.subito.scene.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.scene.ui.start.StartActivity

class SplashActivity : AppCompatActivity() {

    val SPLASH_TIME_OUT: Long = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            val i = Intent(this, StartActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }, SPLASH_TIME_OUT)
    }
}
