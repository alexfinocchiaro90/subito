package com.example.afinocchiaro.subito.network

import com.example.afinocchiaro.subito.MyApplication
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.db.RealmManager
import com.example.afinocchiaro.subito.models.Stargazers
import com.example.afinocchiaro.subito.network.inteface.StartService

/**
 * Created by afinocchiaro on 11/01/2018.
 */

class StartServiceImpl : StartService {

    override fun getAllStargazers(owner: String, repo: String, onSuccess: (List<Stargazers>) -> Unit, onFailure: (String) -> Unit) {
        var list: List<Stargazers>
        // If there is connection download all data
        // And CACHE data on Realm
        if (NetworkConnectionManager.isOnline) {
            StargazersNetworkLayer.getAllStargazers(owner, repo, onSuccess = {
                list = it
                //FLUSH OLD DATA FROM REALM
                RealmManager.instance.flushRealm()

                onSuccess(it)
                //CACHING ON REALM
                RealmManager.instance.saveallStargazers(list)
            }, onFailure = {

                onFailure(it)
            })
        } else {
            //QUERY FROM REALM
            list = RealmManager.instance.retriveallStargazers()
            if(list.isNotEmpty())
            onSuccess(list)
            else onFailure(MyApplication.Companion.INSTANCE!!.getString(R.string.error_msg))
        }
    }

}

