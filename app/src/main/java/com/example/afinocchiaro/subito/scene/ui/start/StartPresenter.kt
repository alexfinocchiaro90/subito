package com.example.afinocchiaro.subito.scene.ui.start

import com.example.afinocchiaro.subito.network.StartRepository

/**
 * Created by afinocchiaro on 07/02/2018.
 */
class StartPresenter(val view: StartActivity, val repository: StartRepository) : StartContract.StartPresenter {

    init {
        view.setPresenter(this)
    }

    override fun start() {
    }

    override fun getAllStargazers(owner: String, repo: String) {

        var list = repository.retriveallStargazers(owner, repo, onSuccess = {

            view.updateView(it)

        }, onFailure = {

            view.showError(it)
        })

    }
}