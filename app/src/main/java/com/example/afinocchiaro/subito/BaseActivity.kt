package com.example.afinocchiaro.axaprototype

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by afinocchiaro on 05/02/2018.
 */

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

}
