package com.example.afinocchiaro.axaprototype

/**
 * Created by afinocchiaro on 05/02/2018.
 */

interface BasePresenter {
    fun start()
}
