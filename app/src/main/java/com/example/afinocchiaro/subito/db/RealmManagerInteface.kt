package com.example.afinocchiaro.subito.db

import com.example.afinocchiaro.subito.models.Stargazers

/**
 * Created by afinocchiaro on 12/01/2018.
 */

interface RealmManagerInteface {


    fun saveallStargazers(stargazersList: List<Stargazers>)

    fun retriveallStargazers(): List<Stargazers>

    fun flushRealm()


}
