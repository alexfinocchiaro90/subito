package com.example.afinocchiaro.subito.scene.ui.detail
import android.os.Bundle
import com.example.afinocchiaro.axaprototype.BaseActivity
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.models.Stargazers
import com.example.afinocchiaro.subito.scene.util.GraphicsUtils
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.custom_view_info_picture.view.*
import kotlinx.android.synthetic.main.custom_view_picture.view.*

class DetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val star : Stargazers =  getIntent().getParcelableExtra(getString(R.string.detail))
        GraphicsUtils.loadImage(this, star.avatarUrl.toString(), cv_picture.iv_photo)
        cv_picture.tv_photo.text = star.login
        cv_info_picture.username.text = star.login
        cv_info_picture.url.text = star.url
        cv_info_picture.num.text = star.id.toString()
    }
}
