package com.example.afinocchiaro.subito.scene.util

import android.app.AlertDialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.afinocchiaro.subito.MyApplication
import com.example.afinocchiaro.subito.R
import com.example.afinocchiaro.subito.scene.ui.start.StartActivity

/**
 * Created by afinocchiaro on 07/02/2018.
 */
class GraphicsUtils {

    companion object {

        fun loadImage(context: Context, url: String, imageView: ImageView) {

            Glide.with(context)
                    .load(url)
                    .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .apply(RequestOptions().placeholder(ColorDrawable(
                            ContextCompat.getColor(MyApplication.INSTANCE!!.applicationContext, R.color.white
                            ))).error(ColorDrawable(
                            ContextCompat.getColor(MyApplication.INSTANCE!!.applicationContext, R.color.red)
                    ))).into(imageView)

        }

        fun showGenericPopUp(activity: StartActivity, msg: String) {
            val dialog: AlertDialog
            var builder: AlertDialog.Builder?
            if (!activity.isFinishing()) {
                builder = AlertDialog.Builder(activity, R.style.MyDialogTheme)
                builder.setMessage(msg)
                        .setTitle(activity.getString(R.string.warning))
                builder.setPositiveButton("OK"
                ) { dialogInterface, i ->
                    // Show location settings when the user acknowledges the alert dialog
                    dialogInterface.dismiss()
                }
                dialog = builder.create()
                dialog.setCancelable(true)
                dialog.setCanceledOnTouchOutside(true)
                dialog.show()
            }
        }
    }


}