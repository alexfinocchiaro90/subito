package com.example.afinocchiaro.subito.scene.ui.start

import com.example.afinocchiaro.axaprototype.BasePresenter
import com.example.afinocchiaro.axaprototype.BaseView
import com.example.afinocchiaro.subito.models.Stargazers

/**
 * Created by afinocchiaro on 07/02/2018.
 */
class StartContract {


     interface StartView : BaseView<StartPresenter> {

        fun updateView(stargazers: List<Stargazers>)

        fun showError(error: String)

    }


     interface  StartPresenter : BasePresenter {

        fun getAllStargazers(owner : String , repo : String)

    }



}